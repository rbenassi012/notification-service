import socket
import random
import string


def message_generator(notification_labels=['Jeddah', 'jeddah', 'LOS ANGELES', 'Casablanca', 'los angelesCasablanca'], min_size=500, chars=string.ascii_uppercase + string.ascii_lowercase + string.digits):
    """
        returns a string  of the specified size as a random message
        :param notification_labels: a list of notification labels to be included in the random message in a random position
        :param min_size: the minimun size of the generated message (number of chars)
        :param chars: the set of properties allowed for the random character generation
    """
    
    message = ''.join(random.choice(chars) for _ in range(min_size))

    if len(notification_labels) >= 2:
        rnd_label_index = random.randint(1, len(notification_labels)-2)
        rnd_message_position = random.randint(0, len(message))
        message = message[0:rnd_message_position] + notification_labels[rnd_label_index] + message[rnd_message_position+1:]

    return message


def main():

    notifications_label = ['jeddah', 'casablanca', 'los angeles', 'jeddahcasablanca']

    try:
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("Connecting to 127.0.0.1")
        client.connect(('127.0.0.1', 30123))

        message = message_generator(notification_labels=notifications_label)
        print(f"Generated message is {message}")

        client.send(message.encode('utf-8'))

        response = client.recv(100)
        message = response.decode(encoding='utf-8')
        print(f"Received message: {message}")

        print("Execution ended")

    except Exception as e:
        print(f"Got exception: {e}")


if __name__ == '__main__':
    main()

from db_controller import DbController


class NotificationService:
    def __init__(self, database, logger):
        self.connectiondb = database
        self.logger = logger

    def handle(self, notification_body, date):
        try:
            with DbController(self.connectiondb, self.logger) as dbconn:
                self.logger.info(
                    f"created the db connection")

                id_customer = None

                self.logger.info(
                    f"Ready to get the customer for the notification {notification_body}")

                customers = dbconn.get_customers_by_notification_body(
                    notification_body)

                self.logger.info(
                    f"Read the notification {notification_body}")

                if len(customers) == 1:
                    # first row -> first column is the id
                    id_customer = customers[0][0]
                    self.logger.info(
                        f"Notification with body {notification_body} and date {date} corresponds to id_customer {id_customer}")
                else:
                    self.logger.info(
                        f"Notification with body {notification_body} matches {len(customers)} customers")

                dbconn.insert_notification(
                    notification_body, date, id_customer)

                self.logger.info(
                    f"Notification with body {notification_body} - {date} - {id_customer} inserted")

        except Exception as e:
            self.logger.error(
                f"Notification service went wrong: exception was {e}")
            raise e

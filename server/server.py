import os
from threading import Thread
from socketserver import ThreadingTCPServer
from listener import TCPHandler
from utils.logger import logger
from settings import  SERVER_PORT,SERVER_THREAD


ThreadingTCPServer.allow_reuse_address = True

with ThreadingTCPServer(('', SERVER_PORT), TCPHandler) as server:

    for n in range(SERVER_THREAD):
        t = Thread(target=server.serve_forever, kwargs={'poll_interval': 1})
        t.daemon = True
        t.start()
    logger.info(f"Started Threads:{SERVER_THREAD}")

    logger.info(f"Waiting for connections on PORT:{SERVER_PORT}...")
    server.serve_forever()

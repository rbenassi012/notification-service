import sqlite3
from sqlite3 import Error

import logging
from utils.logger import getlogger

from settings import DATABASE_NAME


def create_connection(db_file, logger):
    """ create a db connection to sqlite3 specified to the db_file 
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        logger.info(
            f"Create connection to db {db_file} - sqlite3 version {sqlite3.version}")
        return conn
    except Error as e:
        logger.error(f"Failed to connect to {db_file}")
        raise e


def close_connection(conn, logger):
    """ Close the given connection to the databas
    :param conn the connection to the db
    """
    if conn:
        conn.close()
    else:
        logger.error(f"connection cannot be closed: conn is {conn}")


def db_seeding_create_tables(conn, logger, tables):
    """ creates the db schema and insert some fake data into that
    :param conn: the connection to the db
    :param tables: the tables to be created (sql)
    """
    if conn is not None:
        cursor = conn.cursor()
        for table in tables:
            try:
                cursor.execute(table)
            except sqlite3.Error as e:
                logger.error(
                    f"Something went wrong with sql {table}, got expection {e}")
    else:
        logger.error(f"Connection to the db is not available, conn is {conn}")


def db_seeding_insert_fake(conn, logger, entries):
    """ execute sql statement (insert) for populate the db
    :param conn: the connection to the db
    :param entries: the entries to be inserted (sql), 
    """
    # assuming that entries are insert sql instruction, otherwise...

    if conn is not None:
        cursor = conn.cursor()
        for entry in entries:
            try:
                cursor.execute(entry)
            except sqlite3.Error as e:
                logger.error(
                    f"Something went wrong with sql {entry}, got expection {e}")
        conn.commit()
    else:
        logger.error(f"Connection to the db is not available, conn is {conn}")


def db_seeding(dbname, logger, tables, entries):
    """
        prepopulates the given db name
    """
    # no check at the moment if the db alreay exists, create sql statament should include not exists etc
    # or take into account migration (altering data structure, as well as data re-import)

    conn = create_connection(dbname, logger)
    db_seeding_create_tables(conn, logger, tables)
    db_seeding_insert_fake(conn, logger, entries)
    close_connection(conn, logger)


def main():
    logger = getlogger("tcplistener.log", logging.DEBUG)

    # I would like to read the execution statement from a dump? db file rather than hardcoding in this function
    tables = [
        """
        CREATE TABLE IF NOT EXISTS customers (id INTEGER PRIMARY KEY,name TEXT NOT NULL,notification_label TEXT NOT NULL UNIQUE);
        """,
        """
        CREATE TABLE IF NOT EXISTS notifications (id INTEGER PRIMARY KEY AUTOINCREMENT,body TEXT NOT NULL,id_customer INTEGER,FOREIGN KEY (id_customer) REFERENCES customers(id));
        """,
        """
        CREATE TABLE IF NOT EXISTS notification_counters (id_customer INTEGER NOT NULL,num INTEGER NOT NULL DEFAULT 0,day DATE NOT NULL,PRIMARY KEY (id_customer, day),FOREIGN KEY(id_customer) REFERENCES customers(id));
        """
    ]

    # I would like to read the execution statement from a dump? db file rather than hardcoding in this function
    customers = [
        """INSERT INTO customers VALUES(1,'Yvonne Nash','Los Angeles');""",
        """INSERT INTO customers VALUES(2,'Justin Wright','Jeddah');""",
        """INSERT INTO customers VALUES(3,'Thomas Hamilton','Bangkok');""",
        """INSERT INTO customers VALUES(4,'Lily Lee','Casablanca');""",
        """INSERT INTO customers VALUES(5,'Angela Davies','Addis Ababa');""",
        """INSERT INTO customers VALUES(6,'Dan Skinner','Lahore');""",
        """INSERT INTO customers VALUES(7,'Dylan Butler','Kinshasa');""",
        """INSERT INTO customers VALUES(8,'Carl Reid','Dhaka');""",
        """INSERT INTO customers VALUES(9,'Jasmine Rampling','Karachi');""",
        """INSERT INTO customers VALUES(10,'Amelia Ross','Abidjan');""",
    ]

    db_seeding(DATABASE_NAME, logger, tables, customers)


if __name__ == '__main__':
    main()

import logging


def getlogger(filepath, levelinfo):
    file_formatter = logging.Formatter(
        '%(asctime)s~%(levelname)s~%(message)s~module:%(module)s~function:%(funcName)s')
    # console_formatter = logging.Formatter(
    #     '%(asctime)s~%(levelname)s~%(message)s~module:%(module)s~function:%(funcName)s')

    file_handler = logging.FileHandler(filepath)
    file_handler.setLevel(levelinfo)
    file_handler.setFormatter(file_formatter)
    # console_handler = logging.StreamHandler()
    # console_handler.setLevel(levelinfo)
    # console_handler.setFormatter(console_formatter)

    logger = logging.getLogger()
    logger.addHandler(file_handler)
    #logger.addHandler(console_handler)
    logger.setLevel(levelinfo)

    return logger

logger = getlogger("tcplistener.log", logging.DEBUG)
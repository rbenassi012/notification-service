from enum import Enum


class MessageStatus(Enum):
    ACK = 1
    NACK = 2

from db import create_connection, close_connection
import sqlite3

# controller responsibile for db operations


class DbController:
    def __init__(self, connection_db, logger):
        self.connection_db = connection_db
        self.conn = None
        self.logger = logger

    def __enter__(self):
        self.logger.info("Start to create connection")
        self.conn = create_connection(self.connection_db, self.logger)
        self.logger.info("End to create connection")
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        close_connection(self.conn, self.logger)

    def get_customers_by_notification_body(self, notification_body):
        """
            given the nnotification_body returns the ids of the customer that have
            at least one (case insensitive) match with
            :param notification_body: the body of the notification
            :return customers with matching notification
        """
        sql = "SELECT id from customers where :notification_body LIKE ('%'|| notification_label || '%')"
        bindings = {"notification_body": notification_body}

        cursor = self.conn.cursor()

        self.logger.debug(
            f"From {sql} with binding {bindings}")

        cursor.execute(sql, bindings)

        rows = cursor.fetchall()
        cursor.close()
        return rows

    def insert_notification(self, notification_body, day, id_customer=None):
        """
            given an id_customer, a notification body, saves the notification in the notifications tables and
            increments the number of notification per user and per day
            param: notification_body the content of  the notification
            param: day the day in which the notification arrived, formatted as YY-MM-DD
            param: id_customer, the id of the customer

        """
        # each notification label corresponds to a new notification entry into the notifications tables
        # for each notification to be inserted into the notifications tables,
        # a derived data (counter of the notifications per day and per user) must be incremented by 1

        sql_insert_notification = """INSERT INTO notifications("id_customer", "body") values (:id_customer, :body)"""
        bindings_insert_notification = {
            "id_customer": id_customer, "body": notification_body}

        sql_update_counter = """
        INSERT into notification_counters("id_customer", "day", "num") values (:id_customer, :day, 1)
on conflict(id_customer, day) do update
set num = num + 1
        """
        bindings_update_counter = {"id_customer": id_customer, "day": day}

        cursor = self.conn.cursor()

        self.logger.debug(
            f"Ready to execute update counter transaction with insert binding {bindings_insert_notification} and {bindings_update_counter}")

        cursor.execute("BEGIN")
        try:

            self.logger.debug(
                f"Insert notification query {sql_insert_notification} with bindings {bindings_insert_notification}")

            cursor.execute(sql_insert_notification,
                           bindings_insert_notification)

            if id_customer is not None:
                self.logger.debug(
                    f"Insert update query {sql_update_counter} with bindings {bindings_update_counter}")

                cursor.execute(sql_update_counter, bindings_update_counter)

            cursor.execute('COMMIT')

            self.logger.debug(
                f"Committed update counter transaction with insert binding {bindings_insert_notification} and {bindings_update_counter}")

            cursor.close()
        except sqlite3.Error as e:
            cursor.execute("ROLLBACK")
            self.logger.debug(
                f"Rollback update counter transaction with insert binding {bindings_insert_notification} and {bindings_update_counter}")

            cursor.close()
            raise e

    def get_all_notification(self):
        sql = "SELECT id, body, id_customer from notifications"

        cursor = self.conn.cursor()

        self.logger.debug(
            f"query to be executed {sql}")

        cursor.execute(sql)

        rows = cursor.fetchall()
        cursor.close()
        return rows

    def get_all_notification_counters(self):
        sql = "SELECT id_customer, num, day from notification_counters"

        cursor = self.conn.cursor()

        self.logger.debug(
            f"query to be executed {sql}")

        cursor.execute(sql)

        rows = cursor.fetchall()
        cursor.close()
        return rows

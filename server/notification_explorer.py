from db_controller import DbController
import logging
from utils.logger import getlogger
from settings import DATABASE_NAME


# this can be used inside the container to work with the database and check the content
# of notification_counters and notification tables

# just a skeleton, not a full implementation


def main():

    logger = getlogger("notification_explorer.log", logging.DEBUG)

    # we use the same logger but we may choose another logger if needed
    with DbController(DATABASE_NAME, logger) as dbconn:
        notifications = dbconn.get_all_notification()
        logger.info(notifications)

        notifications_counters = dbconn.get_all_notification_counters()
        logger.info(notifications_counters)


if __name__ == '__main__':
    main()

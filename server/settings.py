import os
from os.path import join, dirname
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

# reading the .env configuration
SERVER_PORT = int(os.environ.get('SERVER_PORT'))
SERVER_THREAD = int(os.environ.get('SERVER_THREAD'))
DATABASE_NAME = os.environ.get('DATABASE_NAME')

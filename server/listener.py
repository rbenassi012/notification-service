
import sys
from datetime import date
from socketserver import StreamRequestHandler
from io import BytesIO

from utils.message_status import MessageStatus
from notification_service import NotificationService

from settings import DATABASE_NAME
from utils.logger import logger


class TCPHandler(StreamRequestHandler):

    def handle(self):
        try:
            ip_address = self.client_address[0]
            buffer = BytesIO()

            self.data = self.request.recv(300)
            # not clear if intended that the match has to be done into the 300 chars or
            # the match has to be done in any case in all the content of the message
            # and then truncate to 300 chars only for db recording

            buffer.write(self.data)
            decoded_message = buffer.getvalue().decode(
                encoding='utf-8', errors='ignore')  # do we really need to ignore?

            logger.info(
                f"Server Received Message - {decoded_message} - from ip {ip_address}")

            try:
                NotificationService(DATABASE_NAME, logger).handle(
                    decoded_message, date.today())
                self.request.send(
                    (MessageStatus.ACK.name).encode(encoding='utf-8'))
            except Exception as e:
                logger.error(
                    f"Server was not able to save notification {e}")
                self.request.send(
                    (MessageStatus.NACK.name).encode(encoding='utf-8'))

            self.request.close()

        except ConnectionResetError:
            logger.warning("Connection Reset")
        except Exception:
            logger.exception("Failed to handle request")
        sys.exit(0)

# TCP Server (listener) in Docker Container

Server TCP multi-threaded.
Binary data as input
Send back an ACK 

## Prerequisites

* Docker
* Docker Compose
* Python 3.11 (alpine) -> 64 bit


## Init and Run

To build and start the container, please use the docker compose command 

```
docker-compose  up --build -d
```
It will start a container named tcp_listener, reading some confs from --env-file (which is includes as example)
The port on which the container will listen is defined from the env-file.
```
docker-compose --env-file .\server\.env up --build -d
```
It will start a container named tcp_listener, reading some confs from --env-file (which is includes as example)
The port on which the container will listen is defined from the env-file.

Basic settings in the file:

```
SERVER_PORT=30123
SERVER_THREAD=10
DATABASE_NAME="customers.db"
LOGGING_LEVEL="DEBUG"
LOGGING_FILE="tcplistener.org"
```
## End to End (e2e) testing
Run the client tcp.py to connect to the server

```
python3 client/tcp.py
```
It will generate random strings (by selecting notification_labels that are supposed to have a result) and put them 
into a random position within the length desidered for the message.

##Notes
- docker container init db at startup, at every startup
- db and logging are within the container only. Mapping with host volumes?